import {Component, OnInit} from '@angular/core';
import { House } from '../data/house';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {HouseService} from '../services/house.service';
import { Config, UserService } from '../services';
import { User } from '../data/user';



@Component({
  selector: 'app-house-new',
  templateUrl: './house-new.component.html',
  styleUrls: ['./house-new.component.css']
})

export class HouseNewComponent implements OnInit {

  // Fields
  house: House;
  allHouses: House[];
  loginForm: FormGroup;
  submitted: boolean;
  loading: boolean;
  error: string;

  // Constructor
  constructor(private formBuilder: FormBuilder, private location: Location,
              private houseService: HouseService, private userService: UserService) {}

  // Init
  ngOnInit(): void {
    this.error = '';
    this.getAllHouses();
    this.loginForm = this.formBuilder.group({
      name: ['', Validators.required],
      password1: ['', Validators.required],
      password2: ['', Validators.required],
      grafana: ['']
    });
  }

  // Convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }


  // Submit form
  async onSubmit(): Promise<void> {
    this.submitted = true;

    // Stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }

    if (this.f.password1.value !== this.f.password2.value) {
      this.error = 'Invalid passeword';
      return;
    }

    this.loading = true;

    this.house = new House ({ id: Math.random().toString(36).substr(2, 9),
                              lieu: (this.f.name.value).toUpperCase(),
                              grafana: this.f.grafana.value,
                              password: Config.encrypt(this.f.password1.value),
                              reports: []
     });

    if (this.houseInAllhouses(this.house)) {
      this.loading = false;
      this.error = 'Name already used';
      return;
    }

    await this.houseService.addHouse(this.house);

    // add greenhouse to user's greenhouses
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    currentUser.houses.push(this.house.id);
    console.log(currentUser);
    localStorage.setItem('currentUser', JSON.stringify(currentUser));

    this.userService.updateUser(new User({id: currentUser.id,
                                        firstname: currentUser.firstname,
                                        lastname: currentUser.lastname,
                                        houses: currentUser.houses,
                                        username: currentUser.username
    }));

    this.goBack();
  }

  goBack(): void {
    this.location.back();
  }

  getAllHouses(): void {
    this.houseService.getHouses()
      .subscribe(houses => this.allHouses = houses);
  }

  houseInAllhouses(house: House): boolean {
    for (const s of this.allHouses) {
      if (house.lieu === s.lieu) {
        return true;
      }
    }
    return false;
  }

  resetError(): void {
    this.error = '';
  }

}

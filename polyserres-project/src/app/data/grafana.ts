export class Grafana {

  // Fields
  data: Array<Array<string>>;

  // Constructor
  public constructor(
    fields?: {
        data ?: Array<Array<string>>,
    }) {
    if (fields) {
        Object.assign(this, fields);
    }
}

}


import {Serializable} from './serializable';
import {Report} from './report';

export class House extends Serializable  {

  // Fields
  id: string;
  lieu: string;

  reports: Array<Report>;

  grafana: string;

  password: string | Int32Array;

  // Constructor
  public constructor(
    fields?: {
      id ?: string;
      lieu ?: string;
      reports ?: Array<Report>;
      grafana ?: string;
      password ?: string | Int32Array;
    }) {
    super();
    if (fields) {
        Object.assign(this, fields);
    }
  }
}

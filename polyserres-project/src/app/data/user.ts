﻿import {Serializable} from './serializable';


export class User extends Serializable {

    // Fields
    id: string | Int32Array;

    username: string;
    password: string | Int32Array;

    firstname: string;
    lastname: string;

    email: string;

    houses: string[];

    // Constructor
    public constructor(
        fields?: {
            id ?: string | Int32Array,
            username ?: string,
            password ?: string | Int32Array,
            firstname ?: string,
            lastname ?: string
            houses ?: string[];
        }) {
        super();
        if (fields) {
            Object.assign(this, fields);
        }
    }
}

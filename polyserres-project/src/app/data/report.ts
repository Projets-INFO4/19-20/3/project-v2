import {Serializable} from './serializable';
import Timestamp = firebase.firestore.Timestamp;
import * as firebase from 'firebase';

export class Report extends Serializable  {

  // Fields
  id: string;

  date: Timestamp;
  username: string;

  airTemp: number;
  airHumidity: number;

  outsideAirTemp: number;
  outsideAirHumidity: number;

  waterTemp: number;
  waterPH: number;
  waterConductivity: number;
  waterOximeter: number;

  water: boolean;
  fishies: boolean;
  fertilizer: boolean;

  header: string;
  comment: string;

  // Constructor
  public constructor(
    fields?: {
        id ?: string,

        date ?: Timestamp;
        username ?: string,

        airTemp ?: number,
        airHumidity ?: number;

        outsideAirTemp ?: number;
        outsideAirHumidity ?: number;

        waterTemp ?: number,
        waterPH ?: number,
        waterConductivity ?: number;
        waterOximeter ?: number;

        water ?: boolean,
        fishies ?: boolean,
        fertilizer ?: boolean,

        header ?: string,
        comment ?: string
    }) {
    super();
    if (fields) {
        Object.assign(this, fields);
    }
}

}


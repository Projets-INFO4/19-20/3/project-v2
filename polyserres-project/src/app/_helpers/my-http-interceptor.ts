﻿import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { Config } from '../services/config';
import { UserService } from '../services';

import { User } from '../data/user';

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {

    userService: UserService;
    users: User[];

    constructor(userService: UserService) {
        this.userService = userService;
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        this.userService.getUsers().subscribe(user => this.users = user);

        // wrap in delayed observable to simulate server api call
        return of(null).pipe(mergeMap(() => {

            // authenticate
            if (request.url.endsWith('/users/authenticate') && request.method === 'POST') {
                for (const user of this.users) {
                    if (request.body.username === user.username && Config.encrypt(request.body.password) === user.password) {
                        // if login details are valid return user details
                        const body = {
                            id: user.id,
                            username: user.username,
                            firstname: user.firstname,
                            lastname: user.lastname,
                            houses: user.houses
                        };
                        return of(new HttpResponse({ status: 200, body }));
                    }
                }
                return throwError({ error: { message: 'Username or password is incorrect' } });
            }

            // get users
            if (request.url.endsWith('/users') && request.method === 'GET') {
                // check for auth token in header and return users if valid, this security
                // is implemented server side in a real application
                for (const user of this.users) {
                    if (request.headers.get('Authorization') === `Basic ${window.btoa(user.username + ':' + request.body.password)}`
                    && Config.encrypt(request.body.password) === user.password) {
                        return of(new HttpResponse({ status: 200, body: [user] }));
                    }
                }
                return throwError({ status: 401, error: { message: 'Unauthorised' } });
            }

            // pass through any requests not handled above
            return next.handle(request);

        }))
         .pipe(materialize())
         .pipe(delay(500))
         .pipe(dematerialize());
    }
}

export let myHttpProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: MyHttpInterceptor,
    multi: true
};

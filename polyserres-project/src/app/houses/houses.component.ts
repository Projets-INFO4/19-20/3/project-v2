import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { first } from 'rxjs/operators';

import {House} from '../data/house';
import {HouseService} from '../services/house.service';

import { User } from '../data/user';
import { UserService } from '../services';

import { BasicAuthInterceptor } from '../_helpers';

import { Config } from '../services/config';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-houses',
  templateUrl: './houses.component.html',
  styleUrls: ['./houses.component.css']
})

export class HousesComponent implements OnInit {

  // Fields
  allUsers: User[];
  allHouses: House[] = [];
  user: User;

  // Constructor
  constructor(private houseService: HouseService,
              private userService: UserService,
              private route: ActivatedRoute) { }


  // Init
  ngOnInit(): void {
    this.getCurrentUser();
    this.getAllHouses();
  }

  getAllHouses(): void {
    this.houseService.getHouses()
      .subscribe(houses => this.allHouses = houses);
  }

  inUserHouses(house: House): boolean {
    if (this.user != null && this.user.houses != null &&
         this.user.houses.includes(house.id)) {
      return true;
    }
    return false;
  }

  getUsers(): void {
    this.userService.getUsers()
      .subscribe(user => this.allUsers = user);
// this.userService.getAll().pipe(first()).subscribe(users => {this.users = users;});
  }

  getCurrentUser(): void {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
  }

  getCurrentUserFirstname(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    return currentUser.firstname;
  }

  array(n: number): number[] {
    return Array(n);
  }
}

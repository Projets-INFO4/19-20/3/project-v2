import { Component, OnInit } from '@angular/core';
import {House} from '../data/house';
import {HouseService} from '../services/house.service';
import {ActivatedRoute} from '@angular/router';
import {Report} from '../data/report';
import {ReportService} from '../services/report.service';

@Component({
  selector: 'app-report-detail',
  templateUrl: './report-detail.component.html',
  styleUrls: ['./report-detail.component.css']
})
export class ReportDetailComponent implements OnInit {

  // Fields
  public report: Report;

  // Constructor
  constructor(private reportService: ReportService, private route: ActivatedRoute) {}

  // Init
  ngOnInit(): void {
    this.getReport();
  }

  getReport(): void {
    const idReport = this.route.snapshot.paramMap.get('id');
    this.reportService.getReportById(idReport)
      .subscribe(report => this.report = report);
    console.log(this.report);
  }
}

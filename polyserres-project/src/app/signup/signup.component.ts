import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import { User } from '../data/user';
import { Config } from '../services/config';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit {

  // Fields
  allUsers: User[];
  user: User;
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;

  // Constructor
  constructor(private route: ActivatedRoute, private location: Location,
              private formBuilder: FormBuilder, private userService: UserService) { }

  // Init
  ngOnInit(): void {
    this.getAllUsers();
    this.error = '';
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      password1: ['', Validators.required],
      password2: ['', Validators.required]
  });
  }

   // convenience getter for easy access to form fields
   get f() { return this.loginForm.controls; }

   // Submit
   onSubmit(): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }

    this.loading = true;

     // export into the data base
    this.user = new User({firstname : this.f.firstname.value, lastname : this.f.lastname.value,
      username : this.f.username.value, password : Config.encrypt(this.f.password1.value),
      id : Config.encrypt(this.f.username.value), houses : []});

    if (this.userInAlluserss(this.user)) {
       this.loading = false;
       this.error = 'Username already used';
       return;
     }

    if (this.f.password1.value !== this.f.password2.value) {
       this.loading = false;
       this.error = 'Passeword incorrect';
       return;
    }

    this.userService.addUser(this.user);
    this.location.back();
  }

  getAllUsers(): void {
    this.userService.getUsers()
      .subscribe(users => this.allUsers = users);
  }

  userInAlluserss(user: User): boolean {
    for (const u of this.allUsers) {
      if (user.username === u.username) {
        return true;
      }
    }
    return false;
  }

  resetError(): void {
    this.error = '';
  }
}

import { Component } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {AuthenticationService} from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'Polyserre';

  constructor(private route: ActivatedRoute, private location: Location,
              private authenticationService: AuthenticationService,
              private router: Router) {}

  goBack(): void {
    this.location.back();
  }

  goLogin(): void {
    this.router.navigate([''], {});
  }

  getHome(): string {
    return ('/home/' + JSON.parse(localStorage.getItem('currentUser')).id);
  }

  isHome(): boolean {
     return (this.location.path().includes('home'));
  }

  isLogin(): boolean {
    return (this.location.path() === '');
  }

  authantificationOk(): boolean {
    return JSON.parse(localStorage.getItem('currentUser')) != null;
  }

  isSignUp(): boolean {
    return (this.location.path() === '/signup');
  }

  logOut(): void {
    this.authenticationService.logout();
  }

  myPath(): string {
    return this.location.path();
  }

  isSmartphoneUser(): boolean {
    if (window.innerWidth <= 992) {
      return true;
    }
    return false;
  }
}




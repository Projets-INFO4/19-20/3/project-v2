import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HousesComponent} from './houses/houses.component';
import {HouseDetailComponent} from './house-detail/house-detail.component';
import {ReportDetailComponent} from './report-detail/report-detail.component';
import {ReportNewComponent} from './report-new/report-new.component';
import {ReportEditComponent} from './report-edit/report-edit.component';
import {HouseNewComponent} from './house-new/house-new.component';
import {HouseAddComponent} from './house-add/house-add.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';


const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'home/:id', component: HousesComponent },
  { path: 'new-house', component: HouseNewComponent },
  { path: 'add-house', component: HouseAddComponent },
  { path: 'house/:id', component: HouseDetailComponent },
  { path: 'report/:id', component: ReportDetailComponent },
  { path: 'house/:id/new-report', component: ReportNewComponent },
  { path: 'house/:ids/edit-report/:idr', component: ReportEditComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


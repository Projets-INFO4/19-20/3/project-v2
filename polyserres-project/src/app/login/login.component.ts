import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

    // Fields
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';

    // Consructor
     constructor( private formBuilder: FormBuilder,
                  private route: ActivatedRoute,
                  private router: Router,
                  private authenticationService: AuthenticationService) {
    }


    // Init
     ngOnInit() {
         this.loginForm = this.formBuilder.group({
             username: ['', Validators.required],
             password: ['', Validators.required]
         });

         // reset login status
         this.authenticationService.logout();

         // set returnUrl to home
         this.returnUrl = 'home';
    }

    // [Form] Getter for easy access to form fields
    get f() {
        return this.loginForm.controls;
    }

    // [Form] Submit
    onSubmit() {
         // status
         this.submitted = true;
         this.loading = true;

         // stop here if form is invalid
         if (this.loginForm.invalid) {
             this.loading = false;
             return;
         }

         // Authentification
         this.authenticationService.login(this.f.username.value, this.f.password.value)
             .pipe(first())
             .subscribe(
                 data => {
                    const currentUser  = JSON.parse(localStorage.getItem('currentUser'));
                    this.router.navigate([this.returnUrl + '/' + currentUser.id]);
                },
                 error => {
                    this.error = error;
                    this.loading = false;
                });
    }
}


import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HousesComponent } from './houses/houses.component';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import { HouseDetailComponent } from './house-detail/house-detail.component';
import { ReportDetailComponent } from './report-detail/report-detail.component';
import { ReportNewComponent } from './report-new/report-new.component';
import { ReportEditComponent } from './report-edit/report-edit.component';
import {FormsModule} from '@angular/forms';
import { HouseNewComponent } from './house-new/house-new.component';
import { HouseAddComponent } from './house-add/house-add.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';


import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { BasicAuthInterceptor } from './_helpers/basic-auth.interceptor';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { myHttpProvider } from './_helpers/my-http-interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HousesComponent,
    HouseDetailComponent,
    ReportDetailComponent,
    ReportNewComponent,
    ReportEditComponent,
    HouseNewComponent,
    HouseAddComponent,
    LoginComponent,
    SignupComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase, 'polyserre'), // imports firebase/app needed for everything
    AngularFirestoreModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    // provider used to create fake backend
    myHttpProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { House } from '../data/house';
import { User } from '../data/user';
import { Location } from '@angular/common';
import { ignoreElements } from 'rxjs/operators';
import { UserService } from '../services';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { HouseService } from '../services/house.service';
import { Config } from '../services';



@Component({
  selector: 'app-house-add',
  templateUrl: './house-add.component.html',
  styleUrls: ['./house-add.component.css']
})
export class HouseAddComponent implements OnInit {

  // Fields
  allHouses: House[];
  houseId: string;
  user: User;
  loginForm: FormGroup;
  submitted: boolean;
  loading: boolean;
  error: string;

  // Constructor
  constructor(private location: Location,
              private userService: UserService,
              private formBuilder: FormBuilder,
              private houseService: HouseService) { }

  // Init
  ngOnInit(): void {
    this.getAllHouses();
    this.error = '';
    this.houseId = '';
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.loginForm = this.formBuilder.group({
      name: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  // Convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }


  resetError(): void {
    this.error = '';
  }

  onSubmit(): void {

    this.submitted = true;

    // Stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }

    this.loading = true;

    if (! this.addHouse(this.f.name.value, this.f.password.value)) {
      this.loading = false;
      return;
    }

  }

  addHouse(name: any, password: any): boolean {

    name = name.toUpperCase();

    // Search house name
    let find = false;
    for (const s of this.allHouses) {
      if (s.lieu === name && s.password === Config.encrypt(password)) {
          this.houseId = s.id;
          find = true;
          break;
      }
    }

    if (!find) {
       this.error = 'The name or the password is wrong';
       return false;
    }

    // Adding house

    if (this.user.houses !== []) {
      this.user.houses.push(this.houseId);
    } else {
      this.user.houses = [this.houseId];
    }

    localStorage.setItem('currentUser', JSON.stringify(this.user));
    this.userService.updateUser(this.user);

    this.location.back();

    return true;
  }

  getAllHouses(): void {
    this.houseService.getHouses()
      .subscribe(houses => this.allHouses = houses);
  }

}

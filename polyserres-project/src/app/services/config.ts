import {Md5} from 'ts-md5/dist/md5';

export class Config {

  // API URL
  static apiUrl = 'http://localhost:4200';

  static prefixKey = 'z8df5489e4rf';
  static sufixKey = 'oisjgfr+84gsrg64';

  static encrypt(word: string): string | Int32Array {
    return Md5.hashStr(Config.prefixKey + word + Config.sufixKey);
  }

}

import {Observable} from 'rxjs';
import {House} from '../data/house';
import {map} from 'rxjs/operators';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HouseService {
  houses: Observable<any[]>;
  db: AngularFirestore;

  constructor(db: AngularFirestore) {
    this.db = db;
  }

  getHouses(): Observable<House[]> {

    //
    return this.db.collection<House>('houses')
      .snapshotChanges()
      .pipe(
        map(actions => {

          // @ts-ignore
          return actions.map(a => {

            // Get document data
            const data = a.payload.doc.data();

            // New House
            const house = new House().fromJSON(data);

            // Get document id
            const id = a.payload.doc.id;
            house.id = id;

            // Use spread operator to add the id to the document data
            return house;

          });
        })
      );
  }

  private getHouseDocument(id: string): AngularFirestoreDocument<House> {
    // return document
    return this.db.doc<House>('houses' + `/` + id);
  }

  //
  getHouseById(id: string): Observable<House> {

    // Return house observable
    return this.getHouseDocument(id).snapshotChanges()
      .pipe(
        map(a => {

          // Get document data
          const data = a.payload.data() as House;
          // return {id, ...data};

          // New House
          const house = new House().fromJSON(data);
          house.id = id;
          // Use spread operator to add the id to the document data
          return house;
        })
      );
  }

  updateHouse(house: House) {
    // Update document
    this.getHouseDocument(house.id).update(Object.assign({}, house));
  }

  addHouse(house: House) {
     this.db.collection<House>('houses').doc(house.id).set(Object.assign({}, house));
  }

}

import { Observable } from 'rxjs';
import { Report } from '../data/report';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreDocument, fromDocRef } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import Timestamp = firebase.firestore.Timestamp;
import * as firebase from 'firebase';
import { HouseService } from './house.service';
import { House } from '../data/house';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  report: Observable<Report>;
  db: AngularFirestore;
  houseService: HouseService;
  house: House;

  constructor(db: AngularFirestore, houseService: HouseService) {
    this.db = db;
    this.houseService = houseService;
  }

  private getReportDocument(id: string): AngularFirestoreDocument<Report> {
    // return document
    return this.db.doc<Report>('reports' + '/' + id);
  }

  //
  getReportById(id: string): Observable<Report> {

    // Return report observable
    return this.getReportDocument(id).snapshotChanges()
      .pipe(
        map(a => {

          // Get document data
          const data = a.payload.data() as Report;
          // return {id, ...data};

          // New Report
          const report = new Report().fromJSON(data);
          report.id = id;
          // Use spread operator to add the id to the document data
          // console.log(report);
          return report;
        })
      );
  }

  // Add
  async addReportIntoHouse(report: Report, house: House) {
    this.addReport(report);

    house.reports.push(Object.assign({}, report));
    // appeler house.service.update();
    // pour update les reports de la house
    this.houseService.updateHouse(house);
  }

  // Update
  async updateReportIntoHouse(report: Report, house: House) {
    // Get report
    let i = 0;
    while (i < house.reports.length) {
      if (house.reports[i].id === report.id) {
        // replace report
        house.reports.splice(i, 1);
        house.reports.push(Object.assign({}, report));
        break;
      }
      i++;
    }

    // Update house
    this.houseService.updateHouse(house);

    // Update report
    this.getReportDocument(report.id).update(Object.assign({}, report));
  }

  // Delete
  deleteReport(idr: string, house: House): void {
    // Get report
    let i = 0;
    while (i < house.reports.length) {
      if (house.reports[i].id === idr) {
        // delete report from house
        house.reports.splice(i, 1);
        break;
      }
      i++;
    }

    // Update house
    this.houseService.updateHouse(house);

    // Delete the report from db
    this.getReportDocument(idr).delete();
 }

  //
  addReport(report: Report) {
    this.db.collection<Report>('reports').doc(report.id).set(Object.assign({}, report));
  }

}

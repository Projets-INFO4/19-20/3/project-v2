﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';


import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';



import { User } from '../data/user';
import { Config } from '../services/config';

@Injectable({ providedIn: 'root' })

export class UserService {

    db: AngularFirestore;

    constructor(private http: HttpClient, db: AngularFirestore) {
                    this.db = db;
                 }

    getAll(): Observable<User[]> {
        return this.http.get<User[]>(`${Config.apiUrl}/users`);
    }

    getUsers(): Observable<User[]> {

      return this.db.collection<User>('users')
        .snapshotChanges()
        .pipe(
          map(actions => {

            // @ts-ignore
            return actions.map(a => {

              // Get document data
              const data = a.payload.doc.data();

              // New user
              const user = new User().fromJSON(data);

              // Get document id
              const id = a.payload.doc.id;
              user.id = id;

              // Use spread operator to add the id to the document data
              return user;

            });
          })
        );
    }

    public getUserDocument(id: string | Int32Array): AngularFirestoreDocument<User> {
        // return document
        return this.db.doc<User>('users' + `/` + id);
      }

    getUserById(id: string): Observable<User> {

        // Return user observable
        return this.getUserDocument(id).snapshotChanges()
          .pipe(
            map(a => {

              // Get document data
              const data = a.payload.data() as User;
              // return {id, ...data};

              // New User
              const user = new User().fromJSON(data);
              user.id = id;
              // Use spread operator to add the id to the document data
              return user;
            })
          );
      }

    addUser(user: User) {
        this.db.collection<User>('users').add(Object.assign({}, user));
      }

    updateUser(user: User) {
        this.getUserDocument(user.id).update(Object.assign({}, user));
    }
}

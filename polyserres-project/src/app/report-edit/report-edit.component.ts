import {Component, Input, OnInit} from '@angular/core';
import {Report} from '../data/report';
import {ReportService} from '../services/report.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import Timestamp = firebase.firestore.Timestamp;
import * as firebase from 'firebase';
import {HouseService} from '../services/house.service';
import {House} from '../data/house';


@Component({
  selector: 'app-report-edit',
  templateUrl: './report-edit.component.html',
  styleUrls: ['./report-edit.component.css']
})

export class ReportEditComponent implements OnInit {

  // Fields
  house: House;
  report: Report = null;
  error: string;

  // Constructor
  constructor(private reportService: ReportService, private houseService: HouseService,
              private route: ActivatedRoute,  private location: Location, ) {

  }

  // Init
  ngOnInit(): void {
    this.getHouse();
    this.getReport();
  }

  getReport(): void {
    const idReport = this.route.snapshot.paramMap.get('idr');
    this.reportService.getReportById(idReport)
      .subscribe(report => this.report = report);
  }

  getHouse() {
    this.houseService.getHouseById(this.route.snapshot.paramMap.get('ids')).subscribe(
      house => this.house = house);
  }

  async publierReport(): Promise<void> {

    // validité des intervalles

    if (this.report.airTemp != null && this.report.airTemp < -40 || this.report.airTemp > 60) {
      this.error = 'Inside air temp : wrong value';
      return;
    }

    if (this.report.airHumidity != null && this.report.airHumidity < 0 || this.report.airHumidity > 100) {
      this.error = 'Outside air humidity : wrong value';
      return;
    }

    if (this.report.outsideAirTemp != null && this.report.outsideAirTemp < -40 || this.report.outsideAirTemp > 60) {
      this.error = 'Outside air temp : wrong value';
      return;
    }

    if (this.report.outsideAirHumidity != null && this.report.outsideAirHumidity < 0 || this.report.outsideAirHumidity > 100) {
      this.error = 'Outside air humidity : wrong value';
      return;
    }

    if (this.report.waterTemp != null && this.report.waterTemp < -40 || this.report.waterTemp > 60) {
      this.error = 'Water temp : wrong value';
      return;
    }

    if (this.report.waterPH != null && this.report.waterPH < 1 || this.report.waterPH > 14) {
      this.error = 'Water PH : wrong value';
      return;
    }

    if (this.report.waterConductivity != null && this.report.waterConductivity < 0 || this.report.waterConductivity > 100) {
      this.error = 'Water conductivity : wrong value';
      return;
    }

    if (this.report.waterOximeter != null && this.report.waterOximeter < 0 || this.report.waterOximeter > 100) {
      this.error = 'Water oximeter : wrong value';
      return;
    }

    if (this.report.header == null) {
      return;
    }

    // Publication du report
    await this.reportService.updateReportIntoHouse(this.report, this.house);

    this.goBack();
  }

  goBack(): void {
    this.location.back();
  }

  getCurrentUserFirstname(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    return currentUser.firstname;
  }

}

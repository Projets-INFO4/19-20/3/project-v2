import {Component, Input, OnInit} from '@angular/core';
import {Report} from '../data/report';
import {ReportService} from '../services/report.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import Timestamp = firebase.firestore.Timestamp;
import * as firebase from 'firebase';
import {HouseService} from '../services/house.service';
import {House} from '../data/house';


@Component({
  selector: 'app-report-new',
  templateUrl: './report-new.component.html',
  styleUrls: ['./report-new.component.css']
})

export class ReportNewComponent implements OnInit {

  @Input()

  // Fields
  report: Report;
  house: House;

  fishies: boolean;
  fertilizer: boolean;
  water: boolean;

  airTemp: number;
  airHumidity: number;

  outsideAirTemp: number;
  outsideAirHumidity: number;

  waterTemp: number;
  waterPH: number;
  waterConductivity: number;
  waterOximeter: number;

  header: string;
  comment: string;

  error: string;

  // Constructor
  constructor(private reportService: ReportService, private houseService: HouseService,
              private route: ActivatedRoute,  private location: Location, ) {

  }

  // Init
  ngOnInit(): void {
    this.getHouse();
    this.reset();
  }

  getHouse() {
    this.houseService.getHouseById(this.route.snapshot.paramMap.get('id')).subscribe(
      house => this.house = house);
  }

  async publierReport(): Promise<void> {

        // validité des intervalles

        if (this.airTemp != null && this.airTemp < -40 || this.airTemp > 60) {
          this.reset();
          this.error = 'Inside air temp : wrong value';
          return;
        }

        if (this.airHumidity != null && this.airHumidity < 0 || this.airHumidity > 100) {
          this.reset();
          this.error = 'Outside air humidity : wrong value';
          return;
        }

        if (this.outsideAirTemp != null && this.outsideAirTemp < -40 || this.outsideAirTemp > 60) {
          this.reset();
          this.error = 'Outside air temp : wrong value';
          return;
        }

        if (this.outsideAirHumidity != null && this.outsideAirHumidity < 0 || this.outsideAirHumidity > 100) {
          this.reset();
          this.error = 'Outside air humidity : wrong value';
          return;
        }

        if (this.waterTemp != null && this.waterTemp < -40 || this.waterTemp > 60) {
          this.reset();
          this.error = 'Water temp : wrong value';
          return;
        }

        if (this.waterPH != null && this.waterPH < 1 || this.waterPH > 14) {
          this.reset();
          this.error = 'Water PH : wrong value';
          return;
        }

        if (this.waterConductivity != null && this.waterConductivity < 0 || this.waterConductivity > 100) {
          this.reset();
          this.error = 'Water conductivity : wrong value';
          return;
        }

        if (this.waterOximeter != null && this.waterOximeter < 0 || this.waterOximeter > 100) {
          this.reset();
          this.error = 'Water oximeter : wrong value';
          return;
        }

        if (this.header == null) {
          return;
        }

    // COnstruction du report

        this.report = new Report ({ id: Math.random().toString(36).substr(2, 9),
                                  date: new Timestamp(new Date().getTime() / 1000, 0),
                                  username: JSON.parse(localStorage.getItem('currentUser')).username,
                                  airTemp: this.airTemp,
                                  airHumidity: this.airHumidity,
                                  outsideAirTemp: this.outsideAirTemp,
                                  outsideAirHumidity: this.outsideAirHumidity,
                                  waterTemp: this.waterTemp,
                                  waterPH: this.waterPH,
                                  waterConductivity: this.waterConductivity,
                                  waterOximeter: this.waterOximeter,
                                  water: this.water,
                                  fertilizer: this.fertilizer,
                                  fishies: this.fishies,
                                  header: this.header,
                                  comment: this.comment
                                 });

        // Publication du report
        await this.reportService.addReportIntoHouse(this.report, this.house);

        this.goBack();
  }

  goBack(): void {
    this.location.back();
  }

  getCurrentUserFirstname(): string {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    return currentUser.firstname;
  }

  reset(): void {
    this.waterPH = null;
    this.waterTemp = null;
    this.waterConductivity = null;
    this.waterOximeter = null;
    this.airTemp = null;
    this.airHumidity = null;
    this.outsideAirTemp = null;
    this.outsideAirHumidity = null;
    this.header = null;
    this.comment = null;
    this.fertilizer = false;
    this.fishies = false;
    this.water = false;
    this.error = '';
  }

}

import { Component, OnInit } from '@angular/core';
import {HouseService} from '../services/house.service';
import {House} from '../data/house';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {Report} from '../data/report';
import {Grafana} from '../data/grafana';
import {ReportService} from '../services/report.service';
import { Config } from '../services/config';
import { catchError, map, tap } from 'rxjs/operators';
import {HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-house-detail',
  templateUrl: './house-detail.component.html',
  styleUrls: ['./house-detail.component.css']
})

export class HouseDetailComponent implements OnInit {

  // Fields
  public house: House;
  public grafana: Grafana;

  // Constructor
  constructor(private houseService: HouseService, private reportService: ReportService,
              private route: ActivatedRoute, private http: HttpClient,
              private location: Location) { }


  // Init 
  ngOnInit(): void {
    if (!this.authorizationOk()) {
      this.goBack();
    }
    this.getHouse();
    // this.getGrafana();
  }

  // Authorization to use URL
  authorizationOk(): boolean {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    for (const id of user.houses) {
      if (id === this.route.snapshot.paramMap.get('id')) {
        return true;
      }
    }
    return false;
  }

  goBack(): void {
    this.location.back();
  }

  // WARNING Get grafana doesn't work yet
  getGrafana(): void {

    const header = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin' : 'loclahost:4200',
        Accept : 'application/json',
        'Content-Type' : 'application/json',
        Authorization : 'Bearer eyJrIjoia09vMTJQcDVzMXNlTU80aVA0VjVDMnpQSEdER1FEb3oiLCJuIjoiaW5mbzQtMjAxOSIsImlkIjoxfQ=='
      })
    };

    const url = 'https://polytech.iot.imag.fr/d/fniaung/house-connectee-polytech-jpo?orgId=1' ;
    // const url = 'https://polytech.iot.imag.fr';
    // const url = 'https://polytech.iot.imag.fr/d/fniaung/house-connectee-polytech-jpo?orgId=1&from=1588058555403&to=1588062155403';

    this.http.get<any>(url, header)
    .subscribe(data => {this.grafana = data; });

    // console.log(this.grafana);
  }

  getHouse(): void {
    const idHouse = this.route.snapshot.paramMap.get('id');
    this.houseService.getHouseById(idHouse)
      .subscribe(house => this.house = house);
  }

  array(n: number): number[] {
    return Array(n);
  }

  getCurrentUsername(): string {
    return JSON.parse(localStorage.getItem('currentUser')).username;
  }

  delete(idr: string): void {
    console.log(idr);
    this.reportService.deleteReport(idr, this.house);
    // refresh
  }
}

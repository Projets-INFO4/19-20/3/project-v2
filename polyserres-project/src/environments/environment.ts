// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAaEjsYWBKEkEBD1xGpnw4jo89uRFnOk-Q',
    authDomain: 'polyserre-a6e04.firebaseapp.com',
    databaseURL: 'https://polyserre-a6e04.firebaseio.com',
    projectId: 'polyserre-a6e04',
    storageBucket: 'polyserre-a6e04.appspot.com',
    messagingSenderId: '542738471448',
    appId: '1:542738471448:web:b00d105e7d1f03b1d9016d',
    measurementId: 'G-4SB2MWQWD9'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
